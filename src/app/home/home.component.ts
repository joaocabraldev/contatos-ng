import { Component, OnInit, ViewChild } from '@angular/core';

import { MessageComponent } from '../shared/message/message.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild(MessageComponent)
  private messageComponent: MessageComponent;

  constructor() { }

  ngOnInit(): void {

  }

  enviarMensagem(): void {
    this.messageComponent
      .enviarMensagem({ type: 'warning', text: 'Mensagem Legal!' });
  }

}
