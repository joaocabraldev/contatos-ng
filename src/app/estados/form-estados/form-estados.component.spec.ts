import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEstadosComponent } from './form-estados.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from 'src/app/app-routing.module';

describe('FormEstadosComponent', () => {
  let component: FormEstadosComponent;
  let fixture: ComponentFixture<FormEstadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEstadosComponent ],
      imports: [
        HttpClientModule,
        AppRoutingModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEstadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
