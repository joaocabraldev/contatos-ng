import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { EstadoService } from '../estado.service';
import { Estado } from '../estado';

@Component({
  selector: 'app-form-estados',
  templateUrl: './form-estados.component.html',
  styleUrls: ['./form-estados.component.css']
})
export class FormEstadosComponent implements OnInit {

  estado: Estado = new Estado();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: EstadoService
  ) { }

  ngOnInit(): void {
    this.route.params
    .pipe(
      map((params: any) => params.id),
      switchMap((id) => {
        return id ? this.service.getEstadoPorId(id) : new Observable<Estado>();
      }),
    ).subscribe((estado) => this.estado = estado);
  }

  salvarEstado(): void {
    this.service.salvarEstado(this.estado).subscribe((dados) => {
      this.router.navigate(['/estados']);
    });
  }

}
