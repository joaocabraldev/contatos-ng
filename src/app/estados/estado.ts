export class Estado {

  id: number;
  nome: string;
  sigla: string;

  constructor() {
    this.id = null;
    this.nome = null;
    this.sigla = null;
  }

}
