import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, take } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { Estado } from './estado';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  baseURL = `${environment.API_URL}`;

  constructor(private http: HttpClient) { }

  getEstados(): Observable<Estado[]> {
    const url = `${this.baseURL}/estados`;
    return this.http.get<Estado[]>(url).pipe();
  }

  getEstadoPorId(id: number): Observable<Estado> {
    const url = `${this.baseURL}/estados/${id}`;
    return this.http.get<Estado>(url).pipe(take(1));
  }

  salvarEstado(estado: Estado): Observable<Estado> {

    const saveUrl = `${this.baseURL}/estados`;
    if (estado.id) {
      return this.http.put<Estado>(`${saveUrl}/${estado.id}`, estado).pipe(take(1));
    } else {
      return this.http.post<Estado>(saveUrl, estado).pipe(take(1));
    }

  }

  deletarEstado(id: number): Observable<Estado> {
    const deleteUrl = `${this.baseURL}/estados/${id}`;
    return this.http.delete<Estado>(deleteUrl).pipe(take(1));
  }

}
