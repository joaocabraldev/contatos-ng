import { TestBed } from '@angular/core/testing';

import { EstadoService } from './estado.service';
import { HttpClientModule } from '@angular/common/http';

describe('EstadoService', () => {
  let service: EstadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(EstadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
