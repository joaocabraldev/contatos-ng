import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEstadosComponent } from './lista-estados.component';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';

describe('ListaEstadosComponent', () => {
  let component: ListaEstadosComponent;
  let fixture: ComponentFixture<ListaEstadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaEstadosComponent ],
      imports: [
        HttpClientModule,
        ModalModule.forRoot(),
        AlertModule.forRoot()
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEstadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
