import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { Estado } from '../estado';
import { EstadoService } from '../estado.service';

@Component({
  selector: 'app-lista-estados',
  templateUrl: './lista-estados.component.html',
  styleUrls: ['./lista-estados.component.css']
})
export class ListaEstadosComponent implements OnInit {

  estados$: Observable<Estado[]>;
  estadoSelecionado: Estado;
  modal: BsModalRef;

  constructor(
    private service: EstadoService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.estados$ = this.service.getEstados();
  }

  preDeletar(template: TemplateRef<any>, estado: Estado): void {
    this.estadoSelecionado = estado;
    this.modal = this.modalService.show(template);
  }

  confirm(): void {
    const retorno = this.service.deletarEstado(this.estadoSelecionado.id);
    retorno.subscribe();
    this.estados$ = this.service.getEstados();
    this.estadoSelecionado = new Estado();
    this.modal.hide();
  }

}
