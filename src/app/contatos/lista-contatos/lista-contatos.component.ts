import { Component, OnInit, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';

import { Contato } from '../contato';
import { ContatoService } from '../contato.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-lista-contatos',
  templateUrl: './lista-contatos.component.html',
  styleUrls: ['./lista-contatos.component.css']
})
export class ListaContatosComponent implements OnInit {

  contatos$: Observable<Contato[]>;
  contatoSelecionado: Contato;
  modal: BsModalRef;

  constructor(
    private service: ContatoService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.contatos$ = this.service.getContatos();
  }

  preDeletar(template: TemplateRef<any>, contato: Contato): void {
    this.contatoSelecionado = contato;
    this.modal = this.modalService.show(template);
  }

  confirm(): void {
    const retorno = this.service.deletarContato(this.contatoSelecionado.id);
    retorno.subscribe();
    this.contatos$ = this.service.getContatos();
    this.contatoSelecionado = new Contato();
    this.modal.hide();
  }

}
