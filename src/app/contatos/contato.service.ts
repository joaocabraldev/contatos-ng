import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Contato } from './contato';
import { delay, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContatoService {

  baseURL = `${environment.API_URL}`;

  constructor(private http: HttpClient) { }

  getContatos(): Observable<Contato[]> {
    const url = `${this.baseURL}/contatos`;
    return this.http.get<Contato[]>(url).pipe(
      delay(1000)
    );
  }

  getContatoPorId(id: number): Observable<Contato> {
    const url = `${this.baseURL}/contatos/${id}`;
    return this.http.get<Contato>(url).pipe(take(1));
  }

  salvarContato(contato: Contato): Observable<Contato> {

    const saveUrl = `${this.baseURL}/contatos`;
    if (contato.id) {
      return this.http.put<Contato>(`${saveUrl}/${contato.id}`, contato).pipe(take(1));
    } else {
      return this.http.post<Contato>(saveUrl, contato).pipe(take(1));
    }

  }

  deletarContato(id: number): Observable<Contato> {
    const deleteUrl = `${this.baseURL}/contatos/${id}`;
    return this.http.delete<Contato>(deleteUrl).pipe(take(1));
  }

}
