import { Cidade } from '../cidades/cidade';

export class Contato {
    
    id: number;
    nome: string;
    endereco: string;
    telefone: string;
    cidade: Cidade;

    constructor() {
        this.id = null;
        this.nome = null;
        this.endereco = null;
        this.telefone = null;
        this.cidade = new Cidade();
    }

}