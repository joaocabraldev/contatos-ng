import { Component, OnInit } from '@angular/core';
import { Contato } from '../contato';
import { Observable } from 'rxjs';
import { Estado } from 'src/app/estados/estado';
import { ActivatedRoute, Router } from '@angular/router';
import { Cidade } from 'src/app/cidades/cidade';
import { CidadeService } from 'src/app/cidades/cidade.service';
import { EstadoService } from 'src/app/estados/estado.service';
import { map, switchMap } from 'rxjs/operators';
import { ContatoService } from '../contato.service';

@Component({
  selector: 'app-form-contatos',
  templateUrl: './form-contatos.component.html',
  styleUrls: ['./form-contatos.component.css']
})
export class FormContatosComponent implements OnInit {

  contato: Contato = new Contato();
  estadoSelecionado = new Estado();
  estados$: Observable<Estado[]>;
  cidades$: Observable<Cidade[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ContatoService,
    private serviceCidade: CidadeService,
    private serviceEstado: EstadoService,
  ) { }

  ngOnInit(): void {
    this.route.params.pipe(
      map(params => params.id),
      switchMap(id => this.service.getContatoPorId(id))
    ).subscribe(contato => {
      this.contato = contato;
      this.estados$ = this.serviceEstado.getEstados();
      this.estados$.subscribe(estados => this.carregarCidades());
    });
    
    /*
    this.route.params
    .pipe(
      map((params: any) => params.id),
      switchMap((id) => {
        return id ? this.service.getContatoPorId(id) : new Observable<Contato>();
      }),
    ).subscribe((contato) => {
      this.contato = contato;
      console.log(`Carregando o contato ${this.contato.nome}`);
      this.serviceEstado.getEstados()
      .subscribe((estados) => {
        this.estados$ = estados;
        this.carregarCidades();
      });
    });
    */

    //this.carregarCidades();
  }

  carregarCidades() {
    this.cidades$ = this.serviceCidade.getCidadesPorEstado(this.contato.cidade.estado);
    this.cidades$.subscribe(cidades => console.log(cidades));
  }

  salvarContato(): void {
    this.service.salvarContato(this.contato).subscribe((dados) => {
      this.router.navigate(['/contatos']);
    });
  }

}
