import { TestBed } from '@angular/core/testing';

import { ContatoService } from './contato.service';
import { HttpClientModule } from '@angular/common/http';

describe('ContatoService', () => {
  let service: ContatoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(ContatoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
