import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaEstadosComponent } from './estados/lista-estados/lista-estados.component';
import { ListaCidadesComponent } from './cidades/lista-cidades/lista-cidades.component';
import { ListaContatosComponent } from './contatos/lista-contatos/lista-contatos.component';
import { HomeComponent } from './home/home.component';
import { FormEstadosComponent } from './estados/form-estados/form-estados.component';
import { FormCidadesComponent } from './cidades/form-cidades/form-cidades.component';
import { FormContatosComponent } from './contatos/form-contatos/form-contatos.component';


const routes: Routes = [
  { path: 'estados', component: ListaEstadosComponent },
  { path: 'estados/editar/:id', component: FormEstadosComponent },
  { path: 'estados/novo', component: FormEstadosComponent },
  { path: 'cidades', component: ListaCidadesComponent },
  { path: 'cidades/editar/:id', component: FormCidadesComponent },
  { path: 'cidades/novo', component: FormCidadesComponent },
  { path: 'contatos', component: ListaContatosComponent },
  { path: 'contatos/editar/:id', component: FormContatosComponent },
  { path: 'contatos/novo/:id', component: FormContatosComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
