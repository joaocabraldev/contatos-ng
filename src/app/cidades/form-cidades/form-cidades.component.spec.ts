import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppRoutingModule } from '../../app-routing.module';

import { FormCidadesComponent } from './form-cidades.component';
import { HttpClientModule } from '@angular/common/http';

describe('FormCidadesComponent', () => {
  let component: FormCidadesComponent;
  let fixture: ComponentFixture<FormCidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCidadesComponent ],
      imports: [
        AppRoutingModule,
        HttpClientModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
