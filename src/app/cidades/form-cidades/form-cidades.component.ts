import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { CidadeService } from '../cidade.service';
import { Cidade } from '../cidade';
import { EstadoService } from 'src/app/estados/estado.service';
import { Estado } from 'src/app/estados/estado';

@Component({
  selector: 'app-form-cidades',
  templateUrl: './form-cidades.component.html',
  styleUrls: ['./form-cidades.component.css']
})
export class FormCidadesComponent implements OnInit {

  cidade: Cidade = new Cidade();
  estados$: Observable<Estado[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: CidadeService,
    private serviceEstado: EstadoService,
  ) { }

  ngOnInit(): void {
    this.route.params
    .pipe(
      map((params: any) => params.id),
      switchMap((id) => {
        return id ? this.service.getCidadePorId(id) : new Observable<Cidade>();
      }),
    ).subscribe((cidade) => this.cidade = cidade);

    this.estados$ = this.serviceEstado.getEstados();
    console.log(this.estados$);
  }

  salvarCidade(): void {
    this.service.salvarCidade(this.cidade).subscribe((dados) => {
      this.router.navigate(['/cidades']);
    });
  }

}
