import { Estado } from '../estados/estado';

export class Cidade {

  id: number;
  nome: string;
  capital: boolean;
  estado: Estado;

  constructor() {
    this.id = null;
    this.nome = null;
    this.capital = false;
    this.estado = new Estado();
  }

}
