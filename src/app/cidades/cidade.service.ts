import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cidade } from './cidade';
import { delay, take, filter, map, switchMap } from 'rxjs/operators';
import { Estado } from '../estados/estado';

@Injectable({
  providedIn: 'root'
})
export class CidadeService {

  baseURL = `${environment.API_URL}`;

  constructor(private http: HttpClient) { }

  getCidades(): Observable<Cidade[]> {
    const url = `${this.baseURL}/cidades`;
    return this.http.get<Cidade[]>(url).pipe(
      delay(1000)
    );
  }

  getCidadesPorEstado(estado: Estado): Observable<Cidade[]> {
    const url = `${this.baseURL}/cidades/estado/${estado.id}`;
    return this.http.get<Cidade[]>(url).pipe();
  }

  getCidadePorId(id: number): Observable<Cidade> {
    const url = `${this.baseURL}/cidades/${id}`;
    return this.http.get<Cidade>(url).pipe(take(1));
  }

  salvarCidade(cidade: Cidade): Observable<Cidade> {

    const saveUrl = `${this.baseURL}/cidades`;
    if (cidade.id) {
      return this.http.put<Cidade>(`${saveUrl}/${cidade.id}`, cidade).pipe(take(1));
    } else {
      return this.http.post<Cidade>(saveUrl, cidade).pipe(take(1));
    }

  }

  deletarCidade(id: number): Observable<Cidade> {
    const deleteUrl = `${this.baseURL}/cidades/${id}`;
    return this.http.delete<Cidade>(deleteUrl).pipe(take(1));
  }

}
