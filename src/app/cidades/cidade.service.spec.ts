import { TestBed } from '@angular/core/testing';

import { CidadeService } from './cidade.service';
import { HttpClientModule } from '@angular/common/http';

describe('CidadeService', () => {
  let service: CidadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(CidadeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
