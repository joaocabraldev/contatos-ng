import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCidadesComponent } from './lista-cidades.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule} from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';

describe('ListaCidadesComponent', () => {
  let component: ListaCidadesComponent;
  let fixture: ComponentFixture<ListaCidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCidadesComponent ],
      imports: [
        AppRoutingModule,
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        HttpClientModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
