import { Component, OnInit, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';

import { Cidade } from '../cidade';
import { CidadeService } from '../cidade.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-lista-cidades',
  templateUrl: './lista-cidades.component.html',
  styleUrls: ['./lista-cidades.component.css']
})
export class ListaCidadesComponent implements OnInit {

  cidades$: Observable<Cidade[]>;
  cidadeSelecionada: Cidade;
  modal: BsModalRef;

  constructor(
    private service: CidadeService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.cidades$ = this.service.getCidades();
  }

  preDeletar(template: TemplateRef<any>, cidade: Cidade): void {
    this.cidadeSelecionada = cidade;
    this.modal = this.modalService.show(template);
  }

  confirm(): void {
    const retorno = this.service.deletarCidade(this.cidadeSelecionada.id);
    retorno.subscribe();
    this.cidades$ = this.service.getCidades();
    this.cidadeSelecionada = new Cidade();
    this.modal.hide();
  }

}
