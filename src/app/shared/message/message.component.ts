import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { Message } from './message';
import { MessageService } from './message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  messages: Message[] = [];

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {}

  enviarMensagem(message: Message): void {
    this.messageService.addMessage(message);
    this.messages = this.messageService.getMessages();
  }

  removerMensagem(removedMessage: Message): void {
    this.messageService.removeMessage(removedMessage);
    this.messages = this.messageService.getMessages();
  }

}
