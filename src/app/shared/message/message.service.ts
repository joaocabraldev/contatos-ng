import { Injectable } from '@angular/core';

import { Message } from './message';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private messages: Message[] = [];

  constructor() { }

  getMessages(): Message[] {
    return this.messages;
  }

  addMessage(message: Message): void {
    this.messages.push(message);
  }

  removeMessage(removedMessage: Message): void {
    this.messages.filter((message) => removedMessage !== message);
  }

}
