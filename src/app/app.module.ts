import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';

import { AppRoutingModule } from './app-routing.module';

import { MessageService } from './shared/message/message.service';

import { AppComponent } from './app.component';
import { ListaEstadosComponent } from './estados/lista-estados/lista-estados.component';
import { FormEstadosComponent } from './estados/form-estados/form-estados.component';
import { HomeComponent } from './home/home.component';
import { ListaCidadesComponent } from './cidades/lista-cidades/lista-cidades.component';
import { FormCidadesComponent } from './cidades/form-cidades/form-cidades.component';
import { ListaContatosComponent } from './contatos/lista-contatos/lista-contatos.component';
import { MessageComponent } from './shared/message/message.component';
import { FormContatosComponent } from './contatos/form-contatos/form-contatos.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaEstadosComponent,
    FormEstadosComponent,
    HomeComponent,
    ListaCidadesComponent,
    FormCidadesComponent,
    ListaContatosComponent,
    MessageComponent,
    FormContatosComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    AppRoutingModule
  ],
  providers: [ModalModule, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
