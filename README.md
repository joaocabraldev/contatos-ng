# ContatosNg

Sistema de Contatos usando Angular.
Consome a API usando Java + Spring Boot.

Código fonte da API disponível em: [Contatos Boot API Gitlab](https://gitlab.com/joaocabraldev/contatos-boot-api)
Sistema de API publicado em: [Contatos Boot API Heroku](https://gitlab.com/joaocabraldev/contatos-boot-api)
**Obs:** Devido ao plano gratuito, sistema de API pode estar indisponível e demorar a carregar.

Sistema cliente publicado em: [Contatos NG](https://joaocabraldev.gitlab.io/contatos-ng)